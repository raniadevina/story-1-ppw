from django.shortcuts import render

# Create your views here.
def cover(request):
    return render(request, 'cover.html')
def index(request):
    return render(request, 'index.html')
def interest(request):
    return render(request, 'interest.html')
def campus(request):
    return render(request, 'campus.html')
